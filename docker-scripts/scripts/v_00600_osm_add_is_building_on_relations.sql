-----------------------------------------------------------------------------
-- # Ajout colonne is polygon sur relations : (pour détecter les relations ayant des buildings )
-----------------------------------------------------------------------------
-- inspiré de https://github.com/tyndare/osmose-backend/blob/master/osmosis/WaysCreatePolygon.sql
-- utiliser is_polygone dans requete

ALTER TABLE relations ADD COLUMN is_building boolean DEFAULT FALSE;

WITH relations_building AS
(SELECT relations.id FROM
	relations
	JOIN relation_members ON
		relation_members.relation_id = relations.id AND
		relation_members.member_type = 'W' AND
		relation_members.member_role IN ('', 'outer')
	JOIN ways AS ways ON
		ways.id = relation_members.member_id
	WHERE
		relations.tags->'type' = 'multipolygon'
		AND  (
			relations.tags?'building'
			OR
			(
			ways.tags?'building'
			AND NOT relations.tags?'leisure'
			AND NOT relations.tags?'natural'
			AND NOT relations.tags?'landuse'
			AND NOT ways.tags?'natural'
			)
		)
	GROUP BY relations.id)
UPDATE relations SET is_building = TRUE WHERE id IN (SELECT * FROM relations_building);

CREATE INDEX idx_relations_is_building ON relations USING btree (is_building);
