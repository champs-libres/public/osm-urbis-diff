DROP INDEX IF EXISTS osm_building_polygon;
CREATE INDEX osm_building_polygon ON osm_building USING GIST(polygon);

DROP INDEX IF EXISTS osm_building_polygon_shrinken;
CREATE INDEX osm_building_polygon_shrinken ON osm_building USING GIST(polygon_shrinked);

DROP TABLE IF EXISTS UrbAdm_BUILDING_invalid;
CREATE TABLE  UrbAdm_BUILDING_invalid AS
SELECT * FROM UrbAdm_BUILDING as uab WHERE NOT ST_IsValid(uab.geom);

-- UPDATE UrbAdm_BUILDING AS uab SET geom = ST_MakeValid(uab.geom) WHERE NOT ST_IsValid(uab.geom);
DELETE FROM UrbAdm_BUILDING WHERE NOT ST_IsValid(geom);

CREATE INDEX UrbAdm_BUILDING_building  ON UrbAdm_BUILDING USING GIST(geom);

-- DIFF Between data

DROP TABLE IF EXISTS diff_buildinds;
CREATE TABLE diff_buildinds(
   osm_building geometry(Geometry,4326),
   osm_id bigint,
   osm_tags  hstore,
   urbis_building geometry(MultiPolygon,4326),
   urbis_id integer,
   urbis_versionid integer,
   hausdorff_distance numeric,
   CONSTRAINT pk_results_osm_urbis_id PRIMARY KEY (osm_id, urbis_id)
);

INSERT INTO diff_buildinds
    SELECT
    osm_b.polygon,
    osm_b.id,
    osm_b.tags,
    ua.geom,
    ua.id,
    ua.versionid,
    ST_HausdorffDistance(ST_Transform(ua.geom,4326),ST_Transform(osm_b.polygon,4326))
        FROM UrbAdm_BUILDING AS ua INNER JOIN osm_building as osm_b
            ON ST_Intersects(ua.geom, osm_b.polygon_shrinked)
            WHERE ST_HausdorffDistance(ua.geom,osm_b.polygon) > 0.0015;


-- DETECTER BUILDING DANS 1 MAIS PAS autre
-- batiment dans urbis mais pas dans osm :

DROP TABLE IF EXISTS urbis_no_intersect_osm;
CREATE TABLE urbis_no_intersect_osm AS
SELECT
     ua.geom,
     ua.id,
     ua.versionid,
     count(osm_b.id)
         FROM UrbAdm_BUILDING AS ua LEFT OUTER JOIN osm_building as osm_b
             ON ST_Intersects(ua.geom, osm_b.polygon_shrinked)
         GROUP BY ua.geom, ua.id, ua.versionid HAVING count(osm_b.id) = 0;

-- batiment urbis avec plusieurs intersection osm :
DROP TABLE IF EXISTS urbis_multiple_intersect_osm;
CREATE TABLE urbis_multiple_intersect_osm AS
SELECT
    ua.geom,
    ua.id,
    ua.versionid,
    count(*)
    FROM UrbAdm_BUILDING AS ua INNER JOIN osm_building as osm_b
        ON ST_Intersects(ua.geom, osm_b.polygon_shrinked)
        GROUP BY ua.geom, ua.id, ua.versionid HAVING count(*) > 1;



---- batiments dans osm mais pas dans urbis
DROP TABLE IF EXISTS osm_no_intersect_urbis;
CREATE TABLE osm_no_intersect_urbis AS
SELECT
    osm_b.polygon,
    osm_b.id,
    count(ua.id)
         FROM osm_building as osm_b LEFT OUTER JOIN UrbAdm_BUILDING AS ua
             ON ST_Intersects(ua.geom, osm_b.polygon_shrinked)
         GROUP BY osm_b.polygon, osm_b.id HAVING count(ua.id) = 0;
