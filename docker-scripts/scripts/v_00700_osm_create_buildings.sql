-----------------------------------------------------------
-- CREATION DES buildings qui ne sont pas dans une relation
-----------------------------------------------------------

DROP TABLE IF EXISTS building_not_in_rel;
CREATE TABLE building_not_in_rel
(
    id bigint Not NULL,
    verion integer NOT NULL,
    user_id integer NOT NULL,
    tags hstore,
    polygon geometry(Geometry,4326),
    polygon_shrinked geometry(Geometry,4326),
    CONSTRAINT pk_building_not_in_rel PRIMARY KEY (id)
);

INSERT INTO building_not_in_rel (
WITH ways_in_rel AS
(SELECT w.id FROM
	ways as w
	JOIN relation_members as rm ON
		rm.member_id = w.id AND
		rm.member_type = 'W' AND
		w.is_polygon
    JOIN relations as r ON
        rm.relation_id = r.id AND r.is_building)
SELECT
    w.id, w.version, w.user_id, w.tags, ST_makepolygon(w.linestring), ST_Buffer(ST_MakePolygon(w.linestring), -0.00001)
    FROM ways as w
    WHERE w.id NOT IN (SELECT * FROM ways_in_rel)
    AND w.is_polygon
    AND w.tags ? 'building'
    AND w.tags->'building' != 'no'
);


----------------------------------------------------
-- CREATIONS Des BUILDINS QUI sont dans une relation
----------------------------------------------------

DROP TABLE IF EXISTS building_in_rel;
CREATE TABLE building_in_rel
(
    id bigint Not NULL,
    verion integer NOT NULL,
    user_id integer NOT NULL,
    tags hstore,
    polygon geometry(Geometry,4326),
    polygon_shrinked geometry(Geometry,4326),
    CONSTRAINT pk_building_in_rel PRIMARY KEY (id)
);

INSERT INTO building_in_rel
(WITH outer_ways_in_rel AS
(SELECT w.id FROM
	ways as w
	JOIN relation_members as rm ON
		rm.member_id = w.id AND
		rm.member_type = 'W' AND
        rm.member_role IN ('', 'outer') AND
		w.is_polygon
    JOIN relations as r ON
        rm.relation_id = r.id AND r.is_building)
SELECT w.id, w.version, w.user_id, w.tags, ST_makepolygon(w.linestring), ST_Buffer(ST_MakePolygon(w.linestring), -0.00001)
FROM ways as w WHERE w.id IN (SELECT * FROM outer_ways_in_rel) AND w.is_polygon);


-- RETRAIT DES BUILDINGS INNER QUI SONT DANS UNE RELATION

WITH inner_outer AS
((WITH inner_ways_in_rel AS
(SELECT w.id FROM
	ways as w
	JOIN relation_members as rm ON
		rm.member_id = w.id AND
		rm.member_type = 'W' AND
        rm.member_role IN ('inner') AND
		w.is_polygon)
(SELECT bo.id AS outer_id, COUNT(*) AS count, ST_Union(ST_Makepolygon(bi.linestring)) AS inner_poly FROM building_in_rel bo
	JOIN relation_members as rm1 ON
		rm1.member_id = bo.id
	JOIN relation_members as rm2 ON
		rm2.relation_id = rm1.relation_id
		AND rm2.member_id IN (SELECT * FROM inner_ways_in_rel)
    JOIN ways as bi ON
        rm2.member_id = bi.id AND
        bi.is_polygon
        GROUP BY bo.id HAVING count(*) > 1
        )))
 UPDATE building_in_rel SET polygon = ST_Difference(building_in_rel.polygon, inner_outer.inner_poly)
	FROM inner_outer WHERE inner_outer.outer_id = building_in_rel.id;
