mkdir /data/urbis
unzip /data/UrbAdm_SHP.zip -d /data/urbis/
shp2pgsql -s 31370:4326 /data/urbis/shp/UrbAdm_BUILDING.shp UrbAdm_BUILDING | psql -h "${DATABASE_HOST}" -p "${DATABASE_PORT}" -U postgres
shp2pgsql -s 31370:4326 /data/urbis/shp/UrbAdm_REGION.shp UrbAdm_REGION | psql -h "${DATABASE_HOST}" -p "${DATABASE_PORT}" -U postgres
