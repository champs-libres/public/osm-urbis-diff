#createdb -U postgres osmurbis
psql -h "${DATABASE_HOST}" -p "${DATABASE_PORT}" -U postgres -c "CREATE EXTENSION IF NOT EXISTS hstore;"
#psql --username "$POSTGRES_USER" -d "$POSTGRES_DB" -c "CREATE EXTENSION postgis; CREATE EXTENSION hstore;"
psql -h "${DATABASE_HOST}" -p "${DATABASE_PORT}" -U postgres -f /osmosis/script/pgsnapshot_schema_0.6.sql
psql -h "${DATABASE_HOST}" -p "${DATABASE_PORT}" -U postgres -f /osmosis/script/pgsnapshot_schema_0.6_linestring.sql
osmosis --read-pbf file=/data/brussels_belgium.osm.pbf --bounding-box left=4.2409 top=50.9146 right=4.4849 bottom=50.7608 --write-pgsql database=postgres user=postgres host="${DATABASE_HOST}":"${DATABASE_PORT}"
#osmosis --read-pbf file=/data/brussels_belgium.osm.pbf --tag-filter accept-ways building=* --tag-filter reject-relations --used-node --write-pgsql database=osmurbis user=postgres
