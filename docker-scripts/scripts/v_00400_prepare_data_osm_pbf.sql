-- Create index
CREATE INDEX index_ways_linestring  ON ways USING GIST (linestring);

CREATE INDEX  UrbAdm_Region_geom on UrbAdm_Region USING GIST(geom);

-- REMOVE RELATION OUTSIDE BXL
-- TODO : supprimer les ways des relations -- nope : on les garde

WITH ways_in_re_bxl AS (
   SELECT w.id
       FROM ways AS w
       JOIN UrbAdm_Region
           ON UrbAdm_Region.id = 1
           AND ST_Intersects(w.linestring, UrbAdm_Region.geom)
),
relation_with_elemm_in_re_bel AS(
    SELECT relation_id
       FROM relation_members
       WHERE member_type='W'
       AND member_id IN (SELECT * FROM ways_in_re_bxl)
       GROUP BY relation_id
)
DELETE FROM relations WHERE id NOT IN (SELECT * FROM relation_with_elemm_in_re_bel);

-- Remove ways outsite Brussels
WITH ways_in_rel AS (
    SELECT w.id FROM ways as w JOIN relation_members as rm ON rm.member_id = w.id AND rm.member_type = 'W' group by w.id
),
ways_not_in_bxl_region AS (
    SELECT w.id FROM ways AS w JOIN UrbAdm_Region ON UrbAdm_Region.id = 1 AND NOT ST_Intersects(w.linestring, UrbAdm_Region.geom)
)
DELETE FROM ways as w WHERE w.id IN ((SELECT * FROM ways_not_in_bxl_region) EXCEPT (SELECT * FROM ways_in_rel));
