-----------------------------------------------------------
-- CREATION DES buildings qui ne sont pas dans une relation
-----------------------------------------------------------

DROP TABLE IF EXISTS osm_building;
CREATE TABLE osm_building
(
    id bigint Not NULL,
    verion integer NOT NULL,
    user_id integer NOT NULL,
    tags hstore,
    polygon geometry(Geometry,4326),
    polygon_shrinked geometry(Geometry,4326),
    CONSTRAINT pk_osm_building PRIMARY KEY (id)
);

INSERT INTO osm_building (
    SELECT * FROM building_not_in_rel
);

INSERT INTO osm_building (
    SELECT * FROM building_in_rel
);
