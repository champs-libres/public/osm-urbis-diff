#!/bin/bash
set -e

#export DATABASE_HOST="postgis"
#export DATABASE_PORT="5432"

# waiting for the database to be ready
while ! timeout 1 bash -c "cat < /dev/null > /dev/tcp/${DATABASE_HOST}/${DATABASE_PORT}"
do  
  echo "$(date) : waiting one second for database"; 
  sleep 1; 
done

echo

psql=( psql -h "${DATABASE_HOST}" -p "${DATABASE_PORT}" -U postgres )

echo "ls /scripts/*"

ls /
ls /scripts/*

echo
for f in /scripts/*; do
    echo "running file ${f}";
	case "$f" in
		*.sh)     echo "$0: running $f"; . "$f" ;;
		*.sql)    echo "$0: running $f"; "${psql[@]}" < "$f"; echo ;;
		*)        echo "$0: ignoring $f" ;;
	esac
echo
done

exec "$@"
