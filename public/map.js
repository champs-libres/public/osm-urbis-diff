function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send(); 
}

function loadMap() {
    var url_string = window.location;
    var url = new URL(url_string);
    var searchParams = new URLSearchParams(url.search);
    var mapId = searchParams.get('mapId');

    var map = L.map('map').setView([38.9188702,-77.0708398], 13);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href=”http://osm.org/copyright”>OpenStreetMap</a> contributors'
    }).addTo(map);

    if(mapId) {
        fetchJSONFile('output/' +  mapId + '.geojson', function(data){
            let layer = L.geoJSON(data).addTo(map);
            map.fitBounds(layer.getBounds());
        });
    }
}