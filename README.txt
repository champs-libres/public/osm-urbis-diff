info & data : https://champs-libres.gitlab.io/public/osm-urbis-diff

# Comment utiliser

## Pré-requis

- `docker` et `docker-compose` doivent être installés.

## Compilation et lancement

Depuis la racine du projet : 

- `docker-compose build`
- `docker-compose up` (le script va durer quelques minutes)
- le conteneur `docker-scripts` va s'arrêter au bout de quelques minutes : la base de données est prête.

## Export des données en GeoJSON

Une fois que la base de donnée est prête, il est possible d'exporter 
le résultat en GeoJSON (format qui est lu par JOSM).

```
$ docker-compose run gdal ogr2ogr -f GeoJSON /public/output/urbis_no_intersect_osm.geojson "PG:host=postgis dbname=postgres user=postgres password=postgres" -sql "SELECT geom, id, versionid FROM urbis_no_intersect_osm;"
$ docker-compose run gdal ogr2ogr -f GeoJSON /public/output/osm_no_intersect_urbis.geojson "PG:host=postgis dbname=postgres user=postgres password=postgres" -sql "SELECT polygon, id FROM osm_no_intersect_urbis;"
$ docker-compose run gdal ogr2ogr -f GeoJSON /public/output/urbis_multiple_intersect_osm.geojson "PG:host=postgis dbname=postgres user=postgres password=postgres" -sql "SELECT geom, id, versionid FROM urbis_multiple_intersect_osm;"
```

Les fichiers GeoJSON créés sont alors disponibles dans le dossier `public/output`.


## Accès depuis QGIS

Le serveur `postgresql` est accessible au moyen des paramètres suivants :

- host: `localhost`
- port: `5432`
- username: `postgres`
- db: `postgres`
- password: `postgres`

### Modification de ces paramètres

Le port de la base de donnée peut être modifié dans le fichier `docker-compose.yml`. 

- `cp docker-compose.yml docker-compose.custom.yml`
- Ajoutez vos propres paramètres au fichier `docker-compose.custom.yml`
- exécutez les commandes `docker-compose` avec votre nouveau fichier :
	- `docker-compose -f docker-compose.custom.yml build`
	- `docker-compose -f docker-compose.custom.yml up`

## Erreur : timeout lié à `COMPOSE_HTTP_TIMEOUT`

L'exécution de certaines commandes durent plus de 60 secondes, et docker-compose s'interrompt en raison de leur durée. Veillez à ce que le paramètre `COMPOSE_HTTP_TIMEOUT` soit sur une valeur supérieure.

Pour cela, vérifier que le répertoire à l'intérieur duquel vous exécutez docker-compose contient un fichier `.env` (voir [documentation](https://docs.docker.com/compose/env-file/)).



